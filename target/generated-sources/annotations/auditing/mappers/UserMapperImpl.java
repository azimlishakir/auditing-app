package auditing.mappers;

import auditing.dto.RoleDTO;
import auditing.dto.RoleDTO.RoleDTOBuilder;
import auditing.dto.UsersDTO;
import auditing.dto.UsersDTO.UsersDTOBuilder;
import auditing.model.Role;
import auditing.model.Users;
import auditing.model.Users.UsersBuilder;
import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-10-24T18:46:19+0400",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 1.8.0_251 (Oracle Corporation)"
)
public class UserMapperImpl implements UserMapper {

    @Override
    public UsersDTO toDTO(Users users) {

        UsersDTOBuilder usersDTO = UsersDTO.builder();

        if ( users != null ) {
            if ( users.getRole() != null ) {
                usersDTO.role( roleToRoleDTO( users.getRole() ) );
            }
            if ( users.getPassword() != null ) {
                usersDTO.password( users.getPassword() );
            }
            if ( users.getName() != null ) {
                usersDTO.name( users.getName() );
            }
            if ( users.getId() != null ) {
                usersDTO.id( users.getId() );
            }
            if ( users.getEmail() != null ) {
                usersDTO.email( users.getEmail() );
            }
            if ( users.getLastname() != null ) {
                usersDTO.lastname( users.getLastname() );
            }
            if ( users.getUsername() != null ) {
                usersDTO.username( users.getUsername() );
            }
        }

        return usersDTO.build();
    }

    @Override
    public Users users(UsersDTO usersDTO) {

        UsersBuilder users = Users.builder();

        if ( usersDTO != null ) {
            if ( usersDTO.getRole() != null ) {
                users.role( roleDTOToRole( usersDTO.getRole() ) );
            }
            if ( usersDTO.getPassword() != null ) {
                users.password( usersDTO.getPassword() );
            }
            if ( usersDTO.getName() != null ) {
                users.name( usersDTO.getName() );
            }
            if ( usersDTO.getId() != null ) {
                users.id( usersDTO.getId() );
            }
            if ( usersDTO.getEmail() != null ) {
                users.email( usersDTO.getEmail() );
            }
            if ( usersDTO.getLastname() != null ) {
                users.lastname( usersDTO.getLastname() );
            }
            if ( usersDTO.getUsername() != null ) {
                users.username( usersDTO.getUsername() );
            }
        }

        return users.build();
    }

    protected RoleDTO roleToRoleDTO(Role role) {

        RoleDTOBuilder roleDTO = RoleDTO.builder();

        if ( role != null ) {
            if ( role.getName() != null ) {
                roleDTO.name( role.getName() );
            }
            if ( role.getId() != null ) {
                roleDTO.id( role.getId() );
            }
        }

        return roleDTO.build();
    }

    protected Role roleDTOToRole(RoleDTO roleDTO) {

        Role role = new Role();

        if ( roleDTO != null ) {
            if ( roleDTO.getName() != null ) {
                role.setName( roleDTO.getName() );
            }
            if ( roleDTO.getId() != null ) {
                role.setId( roleDTO.getId() );
            }
        }

        return role;
    }
}
