package auditing.service;

import auditing.dto.UsersDTO;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UsersService extends UserDetailsService {


    List listUsers();

    UsersDTO getUsers(Long id);

    UsersDTO createUsers(UsersDTO usersDTO);

    UsersDTO updateUsers(UsersDTO usersDTO,Long id);



    UsersDTO deleteUsers(Long id);
}