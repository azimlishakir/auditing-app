package auditing.service;

import auditing.dto.RoleDTO;
import auditing.model.Role;

import java.util.List;

public interface RoleService {
    List listRole();

    RoleDTO getRole(Long id);

    RoleDTO createRole(RoleDTO roleDTO);

    RoleDTO updateRole(RoleDTO roleDTO,Long id);

    RoleDTO deleteRole(Long id);
}
