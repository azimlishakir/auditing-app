package auditing.exception;

public class UsersNotFoundIdException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;
    public UsersNotFoundIdException(String message){
        super(String.format("User id must be null %n",message));
    }
}
