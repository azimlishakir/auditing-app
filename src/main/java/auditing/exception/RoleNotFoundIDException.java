package auditing.exception;

public class RoleNotFoundIDException  extends RuntimeException{
    private static final long serialVersionUID = -3042686055658047285L;
    public RoleNotFoundIDException(String message){
        super(String.format("Role id must be null %n",message));
    }
}
