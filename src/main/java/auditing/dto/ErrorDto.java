package auditing.dto;

import java.util.Calendar;

public class ErrorDto {
    private int code;
    private String technicalMessage;
    private String userMessage;
    private Calendar timestamp;

    public ErrorDto() {
    }

    public ErrorDto(int code, String technicalMessage, String userMessage, Calendar timestamp) {
        this.code = code;
        this.technicalMessage = technicalMessage;
        this.userMessage = userMessage;
        this.timestamp = timestamp;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getTechnicalMessage() {
        return technicalMessage;
    }

    public void setTechnicalMessage(String technicalMessage) {
        this.technicalMessage = technicalMessage;
    }

    public String getUserMessage() {
        return userMessage;
    }

    public void setUserMessage(String userMessage) {
        this.userMessage = userMessage;
    }

    public Calendar getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Calendar timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "ErrorDto{" +
                "code=" + code +
                ", technicalMessage='" + technicalMessage + '\'' +
                ", userMessage='" + userMessage + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}