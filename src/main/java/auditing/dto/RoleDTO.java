package auditing.dto;

import auditing.model.Users;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RoleDTO {

    private Long id;

    private String name;
    @JsonIgnore
    private Collection<Users> aUserCollection;

    public RoleDTO(String name) {
        this.name = name;
    }

    public RoleDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "RoleDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", aUserCollection=" + aUserCollection +
                '}';
    }
}
