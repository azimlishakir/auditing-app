package auditing.dto;

import auditing.model.Role;
import lombok.*;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class UsersDTO {
    private Long id;
    private String name;
    private String lastname;
    private String username;
    private String email;
    private String password;
    private RoleDTO role;
}
