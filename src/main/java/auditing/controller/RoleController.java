package auditing.controller;

import auditing.dto.RoleDTO;
import auditing.model.Role;
import auditing.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/role")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping("all")
    public ResponseEntity allRole(){
        return ResponseEntity.ok(roleService.listRole());
    }

    @GetMapping("{id}")
    public ResponseEntity<RoleDTO> getRole(@PathVariable("id") Long id){
        return ResponseEntity.ok(roleService.getRole(id));
    }

    @PostMapping
    public ResponseEntity<RoleDTO> createRole(@RequestBody RoleDTO roleDTO){
        System.out.println ("Role elave edildi " + roleDTO);
        return ResponseEntity.ok(roleService.createRole(roleDTO));
    }

    @PutMapping("{id}")
    public ResponseEntity<RoleDTO> updateRole(@RequestBody RoleDTO roleDTO,@PathVariable("id") Long id){
        return ResponseEntity.ok(roleService.updateRole(roleDTO,id));
    }

    @DeleteMapping("{id}")
    ResponseEntity<RoleDTO> deleteRole(@PathVariable("id") Long id){
        return ResponseEntity.ok(roleService.deleteRole(id));
    }



}
