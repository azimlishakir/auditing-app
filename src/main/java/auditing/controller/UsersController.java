package auditing.controller;


import auditing.dto.UsersDTO;
import auditing.service.UsersService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/users")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(value = "Users API")
public class UsersController {

    @Autowired
    private UsersService usersService;

    @GetMapping("all")
    public ResponseEntity allUsers(){
        return ResponseEntity.ok (usersService.listUsers ());
    }

    @GetMapping("{id}")
    public ResponseEntity<UsersDTO> getUsers(@PathVariable("id") Long id){
        return ResponseEntity.ok (usersService.getUsers (id));
    }

    @PostMapping
    public ResponseEntity<UsersDTO> createUsers(@Valid @RequestBody UsersDTO usersDTO){
        System.out.println ("Users elave edildi " + usersDTO);
        return ResponseEntity.ok (usersService.createUsers(usersDTO));
    }

    @PutMapping("{id}")
    public ResponseEntity<UsersDTO> updateUsers(@RequestBody UsersDTO usersDTO, @PathVariable("id") Long id){
        return ResponseEntity.ok (usersService.updateUsers (usersDTO,id));
    }

    @DeleteMapping("{id}")
    ResponseEntity<UsersDTO> deleteUsers(@PathVariable("id") Long id, UsersDTO usersDTO){
        usersService.deleteUsers (id);
        return ResponseEntity.ok(usersDTO);
    }
}