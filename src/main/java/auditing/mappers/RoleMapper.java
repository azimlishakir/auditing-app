package auditing.mappers;

import auditing.dto.RoleDTO;
import auditing.model.Role;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface RoleMapper {
    RoleMapper INSTANCE = Mappers.getMapper(RoleMapper.class);


    @Mappings({
            @Mapping(target = "id", source = "role.id"),
            @Mapping(target = "name", source = "role.name"),
    })

    RoleDTO roleToDTO (Role role);



    Role role(RoleDTO roleDTO);

}
