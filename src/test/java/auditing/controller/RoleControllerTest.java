package auditing.controller;


import auditing.dto.RoleDTO;
import auditing.service.RoleService;
import auditing.service.UsersService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import java.util.Collections;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(RoleController.class)
@ActiveProfiles("default")
@WithMockUser
public class RoleControllerTest {

    private static final String API = "/role";
    private static final String APIBYID = "/role/{id}";
    private static final String APIALL = "/role/all";

    private static final String ID = "$.id";
    private static final String NAME = "$.name";


    private static final long ROLE_ID = 1;

    @MockBean
    private RoleService roleService;

    @MockBean
    private UsersService usersService;



    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    private RoleDTO roleDTO;

    @BeforeEach
    public void setUp() {
        roleDTO = RoleDTO.builder().
                id(ROLE_ID).
                name("ADMINISTRATOR").build();

    }

    @Test
     void allRole() throws Exception{
        when(roleService.listRole()).thenReturn(Collections.EMPTY_LIST);
        mvc.perform( get(APIALL)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    public void getRole() throws Exception{

        when(roleService.getRole(ROLE_ID)).thenReturn(roleDTO);

        mvc.perform(get(APIBYID,ROLE_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(roleDTO.getId()));
    }



    @Test
    public void createRole() throws Exception{
        when(roleService.createRole(roleDTO)).thenReturn(roleDTO);
        mvc.perform(post(API)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(roleDTO))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(ID).value(roleDTO.getId()))
                .andExpect(jsonPath(NAME).value(roleDTO.getName()))
                .andDo(print());
    }



    @Test
    public void updateRole() throws Exception{
        when(roleService.updateRole(roleDTO,ROLE_ID)).thenReturn(roleDTO);
        mvc.perform(put(APIBYID,ROLE_ID)
                .content(objectMapper.writeValueAsString(roleDTO))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath(ID).value(roleDTO.getId()))
                .andExpect(jsonPath(NAME).value(roleDTO.getName()))
                .andDo(print())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

   @Test
    public void deleteRole() throws Exception{
        when(roleService.deleteRole(ROLE_ID)).thenReturn(roleDTO);
        mvc.perform(delete(APIBYID,ROLE_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath(ID).value(roleDTO.getId()));
    }

}