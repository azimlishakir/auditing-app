FROM openjdk:8-jdk-alpine
ADD target/*.jar *.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "*.jar"]
    
